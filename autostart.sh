#!/usr/bin/env bash 
lxsession &
killall picom &>/dev/null
picom --config $HOME/.config/qtile/picom.conf &
xsetroot -cursor_name left_ptr &
xset r rate 200 30 &
killall applet.py &
feh --bg-scale Imágenes/Wallpapers/303155.jpg
killall pnmixer &>/dev/null
pnmixer &
nm-applet &
xbindkeys &
killall imwheel &>/dev/null
imwheel -b 45 &
~/.scripts/gsimplecal/gsimplecal.sh &
/usr/bin/lxqt-policykit-agent &
killall mpd &>/dev/null
mpd &
sleep 30 && ~/.scripts/checkmail/checkmail-cron.sh &
sleep 30 && ~/.scripts/checkcal/checkcal-cron.sh &
# sleep 20 && /usr/bin/emacs --daemon &
nextcloud &
