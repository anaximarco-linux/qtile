import os
import re
import socket
import subprocess
import owm
from libqtile import qtile
from libqtile.config import Click, Drag, Group, ScratchPad, DropDown, KeyChord, Key, Match, Screen
from libqtile.command import lazy
from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook
from libqtile.widget import (Prompt)
from typing import List # noqa: F401
from os import path

@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

@lazy.function
def window_to_prev_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

@lazy.function
def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

@lazy.function
def switch_screens_prev(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

@lazy.function
def switch_screens_next(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i + 1].group
    qtile.current_screen.set_group(group)

qtile_path = path.join(path.expanduser('~'), ".config", "qtile")

HOME = os.environ['HOME']                                                       # Sets user home directory
mod = "mod4"                                                                    # Sets mod key to SUPER/WIN
terminal = "alacritty"                                                                   # First terminal
                                       # Emacs Client
# myEmacs = "emacsclient -c -a emacs --eval '(dashboard-refresh-buffer)'"         # Emacs Client

myCity = "Oax"              # City name in widget
myCityCode = "oaxaca"      # City name for wheather widget localization
myLatitude = "17.0654"     # City latitude for weather widget icon
myLongitude = "-96.723"     # City longitude for weather widget icon
open_weatherapi_key = "7834197c2338888258f8cb94ae14ef49"

keys = [
    Key([mod], "h", lazy.layout.left(), desc="Move focus left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod],
        "space",
        lazy.layout.next(),
        desc="Move window focus to other window"),

    Key([mod], "r", lazy.spawn("rofi -show combi"), desc="spawn rofi"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"],
        "h",
        lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"],
        "l",
        lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"],
        "j",
        lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"],
        "h",
        lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"],
        "l",
        lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"],
        "j",
        lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(f"{terminal}"), desc="Launch terminal"),
    Key([mod], "f", lazy.spawn("firefox"), desc="Lanza firefox enjaulado"),
    Key([mod], "d", lazy.spawn("thunar"), desc="Lanza thunar"),
    Key([mod], "g", lazy.spawn("firefox -private-window"), desc="Firefox privado"),
    Key([mod], "t", lazy.spawn("texstudio"), desc="Lanza texstudio"),
    Key([mod], "c", lazy.spawn("chromium"), desc="Lanza chromium"),
    Key([mod], "o", lazy.spawn("openttd"), desc="Lanza openttd"),
    Key([mod], "s", lazy.spawn("spotify"), desc="Lanza spotify"),

    #Acordes de teclas para terminal
    KeyChord([mod], "m", [
        Key([], "n", lazy.spawn(f"{terminal} -e neomutt")),
        Key([], "d", lazy.spawn(f"{terminal} -e ranger")),
        Key([], "c", lazy.spawn(f"{terminal} -e calcurse")),
        Key([], "b", lazy.spawn(f"{terminal} -e bpytop")),
	Key([], "s", lazy.spawn(f"{terminal} -e .scripts/spoti.sh")),
    ]),

    #script para spotify-tui
    #!/bin/bash <---- la almohadilla es parte del código
    #
    #killall spotifyd
    #spotifyd
    #spt

    #Acordes para aplicaciones
    KeyChord([mod], "a", [
        Key([], "t", lazy.spawn("flatpak run com.github.zadam.trilium")),
        Key([], "g", lazy.spawn("gimp")),
        Key([], "k", lazy.spawn("keepass")),
        Key([], "r", lazy.spawn("org.libretro.RetroArch")),
        Key([], "v", lazy.spawn("code")),
        Key([], "s", lazy.spawn("steam")),
        Key([], "h", lazy.spawn("thunderbird")),
        Key([], "f", lazy.spawn("./.applications/fet-6.3.0-x86_64.AppImage")), #FET timetabling
        Key([], "m", lazy.spawn("/usr/local/Wolfram/Mathematica/13.1/Executables/Mathematica")),
    ]),

    #Acordes extras
    KeyChord([mod], "e", [
        Key([], "t", lazy.spawn("telegram-desktop")),
        Key([], "k", lazy.spawn("kdeconnect-app")),
        Key([], "f", lazy.spawn("torbrowser-launcher")),
    ]),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "shift", "control"], "h", lazy.layout.swap_column_left()),
    Key([mod, "shift", "control"], "l", lazy.layout.swap_column_right()),
    Key([mod, "shift"], "space", lazy.layout.flip()),
    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod, "shift"],
        "r",
        lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),
    Key([], "XF86AudioRaiseVolume",lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%")),
    Key([], "XF86AudioLowerVolume",lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%")),
    Key([], "XF86AudioMute",lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle")),
    #Control del brillo
    Key([], "XF86MonBrightnessUp",lazy.spawn("brightnessctl set +10%")),
    Key([], "XF86MonBrightnessDown",lazy.spawn("brightnessctl set 10%-")),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),  #Necesario instalar playerctl
    Key([], "Print",lazy.spawn("escrotum /home/anaximarco/Imágenes/Screenshots/%Y-%m-%d-%H%M%S.png")),
    Key(["shift"], "Print",lazy.spawn("escrotum -s /home/anaximarco/Imágenes/Screenshots/%Y-%m-%d-%H%M%S.png")),
]

### END_KEYS

group_names = [("ANY", {'layout': 'monadtall'}),
               ("WEB", {'layout': 'monadtall'}),
               ("WRK", {'layout': 'monadtall', 'matches':[Match(wm_class=["TeXstudio"])]}),
               ("DOC", {'layout': 'monadtall'}),
               ("IMA", {'layout': 'monadtall'}),
               ("AUD", {'layout': 'monadtall'}),
               ("GAM", {'layout': 'monadtall', 'matches':[Match(wm_class=["Inkscape"])]}),
               ("MSG", {'layout': 'monadtall', 'matches':[Match(title=["Telegram"])]}),
               ("SPT", {'layout': 'monadtall', 'matches':[Match(wm_class=["spotify"])]})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

groups.append(
    ScratchPad("scratchpad", [
        # define a drop down terminal.
        DropDown("term", "st", x=0.15, y=0.15, width=0.7, height=0.7, opacity=1.0),
        # define a drop down notepad.
        DropDown("note", "leafpad", x=0.3, y=0.3, width=0.4, height=0.4, opacity=1.0),
        # define a drop down calculator.
        DropDown("calc", "qalculate-gtk", x=0.3, y=0.3, width=0.4, height=0.4, opacity=1.0) ])),

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen(toggle=True), lazy.next_screen(), lazy.next_screen(), lazy.next_screen(), lazy.spawn(HOME+"/.config/qtile/scripts/mousemove")))         # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name)))             # Send current window to another group

layout_theme = {"border_width": 2,
                "single_border_width": 2,
                "margin": 10,
                "border_normal": "#1e1f29",
                "border_focus": "#00ff00"
                }

layouts = [
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Tile(**layout_theme, shift_windows=True),
    layout.Floating(**layout_theme)
    # layout.RatioTile(**layout_theme),
    # layout.VerticalTile(**layout_theme),
    # layout.Matrix(**layout_theme),
    # layout.Columns(**layout_theme),
    # layout.Zoomy(**layout_theme),
    # layout.Stack(border_width=2, margin=[10,10,10,10], border_focus="a16ae8", border_normal="1e1f29", num_stacks=3),
    # layout.Max(**layout_theme),
]

colors = [["#1e1f29", "#1e1f29"], # 0 dark grey ("panel" background - in fact, widgets background)
          ["#4f596e", "#4f596e"], # 1 mid grey (highlighted tag)
          ["#ffffff", "#ffffff"], # 2 white (foreground)
          ["#fd49a0", "#fd49a0"], # 3 pink (widgets)
          ["#a16ae8", "#a16ae8"], # 4 light purple (widgets)
          ["#fbc640", "#fbc640"], # 5 yellow (tags, widgets)
          ["#46d9ff", "#46d9ff"], # 6 cyan (widgets)
          ["#b5b5b5", "#b5b5b5"]] # 7 light grey (window name widget)

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

widget_defaults = dict(
    background = colors[0],
    foreground = colors[2],
    font = "Ubuntu Bold",
    fontsize = 10,
    padding = 0
)
extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       ),
              widget.Image(
                       scale = True,
                       margin = 4.5,    #(4.5 for Arch logo | 6.5 for Qtile logo)
                       filename = "~/.config/qtile/icons/menu.png",
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(HOME+"/.config/rofi/launchers/colorful/launcher-full.sh"), 'Button3': lambda: qtile.cmd_spawn(HOME+"/.sources/xmenu/xmenu.sh")}
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 2,
                       ),
              widget.TextBox(
                       text = '⠇',
                       font = "Ubuntu",
                       fontsize = 14
                       ),
              widget.GroupBox(
                       disable_drag = True,
                       use_mouse_wheel = False,
                       margin_y = 4,
                       margin_x = 0,
                       padding_y = 5,
                       padding_x = 3,
                       borderwidth = 3,
                       active = colors[5],
                       inactive = colors[2],
                       rounded = True,
                       highlight_color = colors[1],
                       highlight_method = "line",
                       this_current_screen_border = colors[3],
                       this_screen_border = colors [3],
                       other_current_screen_border = colors[6],
                       other_screen_border = colors[4]
                       ),
              widget.TextBox(
                       text = '',
                       foreground = colors[0],
                       background = "#1e1f2900",
                       font = "mononoki Nerd Font",
                       fontsize = 25
                       ),
              widget.Sep(
                       background = ["#1e1f2900"],
                       linewidth = 0,
                       padding = 10
                       ),

              widget.TextBox(
                       text = '',
                       foreground = colors[0],
                       background = "#1e1f2900",
                       font = "mononoki Nerd Font",
                       fontsize = 25
                       ),
              widget.Prompt(
                       foreground = colors[5],
                       prompt = ">>> ",
                       cursor_color = 'fbc640',
                       fontsize = 11,
                       padding = 0,
                       ignore_dups_history = True
                       ),
              widget.CurrentLayoutIcon(
                       custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                       scale = 0.40,
                       ),
              widget.WindowCount(
                      foreground = colors[7],
                      show_zero = True,
                      text_format = '  {num} |≋ ',
                      ),
              widget.WindowName(
                       format = '{name}',
                       foreground = colors[7],
                       mouse_callbacks = {'Button1': lambda: qtile.current_window.kill(), 'Button2': lazy.window.toggle_floating(), 'Button3': lazy.window.toggle_floating()}
                       ),
              widget.Sep(
                       background = ["#1e1f29"],
                       linewidth = 0,
                       padding = 10
                       ),
              widget.GenPollText(
                       foreground = "#ffffff",
                       update_interval = 5,
                       font = "mononoki Nerd font",
                       fontsize = 11,
                       markup = False,
                       func = lambda: subprocess.check_output(HOME+"/.config/qtile/scripts/checkmail-trash").decode("utf-8"),
                       ),
              widget.GenPollText(
                       foreground = "#00ff00",
                       update_interval = 2,
                       max_chars = 65,
                       markup = False,
                       func = lambda: subprocess.check_output(HOME+"/.config/qtile/scripts/mpd").decode("utf-8"),
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('mpc toggle'), 'Button3': lambda: qtile.cmd_spawn('mpc next'), 'Button2': lambda: qtile.cmd_spawn('mpc stop')},
                       ),
              widget.GenPollText(
                       foreground = "#00ff00",
                       update_interval = 2,
                       max_chars = 50,
                       markup = False,
                       func = lambda: subprocess.check_output(HOME+"/.config/qtile/scripts/spotify").decode("utf-8"),
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('dbus-send --print-reply --dest=org.mpris.MediaPlayer2.ncspot /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause'), 'Button3': lambda: qtile.cmd_spawn('dbus-send --print-reply --dest=org.mpris.MediaPlayer2.ncspot /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next')}
                       ),
              widget.TextBox(
                       text = '',
                       foreground = colors[0],
                       background = "#1e1f2900",
                       font = "mononoki Nerd Font",
                       fontsize = 25
                       ),
              widget.Sep(
                       background = ["#1e1f2900"],
                       linewidth = 0,
                       padding = 10
                       ),

              widget.Systray(
                       background = "#1e1f2900",
                       icon_size = 14,
                       padding = 7
                       ),

              widget.TextBox(
                       text = '',
                       foreground = colors[0],
                       background = "#1e1f2900",
                       font = "mononoki Nerd Font",
                       fontsize = 25
                       ),
              widget.TextBox(
                       text = "菱 ",
                       font = "mononoki Nerd Font",
                       foreground = colors[6],
                       fontsize = 13
                       ),
              widget.GenPollText(
                       foreground = colors[6],
                       update_interval = 600,
                       markup = False,
                       func = lambda: subprocess.check_output(HOME+"/.config/qtile/scripts/pacupdate").decode("utf-8"),
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e sudo pacman -Syu'), 'Button3': lambda: qtile.cmd_spawn(myTerm + ' -e yay -Sua')}
                       ),
              widget.TextBox(
                       text = '|',
                       foreground = colors[7],
                       padding = 6,
                       font = "Source Code Pro",
                       fontsize = 12
                       ),

              widget.TextBox(
                       font = "mononoki Nerd Font",
                       text = " ",
                       foreground = colors[5],
                       fontsize = 12
                       ),
              widget.TextBox(
                       text = "  KeyLocks   ",
                       foreground = colors[5],
                       ),
              widget.GenPollText(
                       foreground = colors[5],
                       fontsize = 9,
                       update_interval = 1,
                       font = "mononoki Nerd font",
                       markup = False,
                       func = lambda: subprocess.check_output(HOME+"/.config/qtile/scripts/capsnum").decode("utf-8"),
                       ),
              widget.TextBox(
                       text = '|',
                       foreground = colors[7],
                       padding = 6,
                       font = "Source Code Pro",
                       fontsize = 12
                       ),

              widget.TextBox(
                       text = " ",
                       foreground = colors[4],
                       font = "Mononoki Nerd Font"
                       ),
              widget.Net(
                       format = '{down}',
                       prefix = "k",
                       foreground = colors[4],
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('nm-connection-editor')}
                       ),
              widget.TextBox(
                       text = "  ",
                       foreground = colors[4],
                       fontsize = 15
                       ),
              widget.TextBox(
                       text = " ",
                       foreground = colors[4],
                       font = "Mononoki Nerd Font"
                       ),
              widget.Net(
                       format = '{up}',
                       prefix = "k",
                       foreground = colors[4],
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('nm-connection-editor')}
                       ),
              widget.TextBox(
                       text = '|',
                       foreground = colors[7],
                       padding = 6,
                       font = "Source Code Pro",
                       fontsize = 12
                       ),

              widget.TextBox(
                       font = "mononoki Nerd Font",
                       text = " ",
                       foreground = colors[6],
                       fontsize = 12,
                       ),
              widget.ThermalSensor(
                    foreground = colors[6],
                    tag_sensor = 'edge'
                ),
              # widget.CPU(
              #          foreground= colors[6],
              #          update_interval = 2,
              #          format = '  {freq_current} GHz | {load_percent}%',
              #          mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e gtop')},
              #          ),
              widget.TextBox(
                       text = '|',
                       foreground = colors[7],
                       padding = 6,
                       font = "Source Code Pro",
                       fontsize = 12
                       ),

              widget.TextBox(
                       font = "Mononoki Nerd Font",
                       text = " ",
                       foreground = colors[5],
                       fontsize = 12,
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')}
                       ),
              widget.Memory(
                       foreground = colors[5],
                       format = ' {MemUsed: .0f}{mm} /{MemTotal: .0f}{mm}',
                       measure_mem = "M",
                       update_interval = 2,
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')}
                       ),
              widget.TextBox(
                       text = '|',
                       foreground = colors[7],
                       padding = 6,
                       font = "Source Code Pro",
                       fontsize = 12
                       ),              
              widget.BatteryIcon(
                ),
                widget.Battery(
                    format = "{percent:2.0%}",
                    foreground = colors[4]
                ),
              widget.TextBox(
                       text = '|',
                       foreground = colors[7],
                       padding = 6,
                       font = "Source Code Pro",
                       fontsize = 12
                       ),

              widget.TextBox(
                      foreground = colors[3],
                      text = myCity+"  ",
                      mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm+" -e "+HOME+"/.config/qtile/scripts/weather-click")}
                      ),
              owm.OpenWeatherMap(
                       api_key = open_weatherapi_key,
                       latitude = myLatitude,
                       longitude = myLongitude,
                       foreground = colors[3],
                       icon_font = "Weather Icons",
                       fontsize = 11,
                       format = '{icon} ',
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm+" -e "+HOME+"/.config/qtile/scripts/weather-click")}
                       ),
              widget.OpenWeather(
                       foreground = colors[3],
                       update_interval = 1800,
                       location = myCityCode,
                       format = '{temp:.0f}° {units_temperature}   ',
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm+" -e "+HOME+"/.config/qtile/scripts/weather-click")}
                       ),
              widget.TextBox(
                       font = "mononoki Nerd Font",
                       text = "",
                       foreground = colors[3],
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm+" -e "+HOME+"/.config/qtile/scripts/weather-click")}
                       ),
              widget.OpenWeather(
                       foreground = colors[3],
                       format = '  {humidity}%',
                       update_interval = 1800,
                       location = "goiania",
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm+" -e "+HOME+"/.config/qtile/scripts/weather-click")}
                       ),
              widget.TextBox(
                       text = '',
                       foreground = colors[0],
                       background = "#1e1f2900",
                       font = "mononoki Nerd Font",
                       fontsize = 25
                       ),
              widget.Sep(
                       background = ["#1e1f2900"],
                       linewidth = 0,
                       padding = 10
                       ),

              widget.TextBox(
                       text = '',
                       foreground = colors[0],
                       background = "#1e1f2900",
                       font = "mononoki Nerd Font",
                       fontsize = 25
                       ),
              widget.TextBox(
                  text = '墳',
                  foreground = colors[2],
                  padding = 0,
                  fontsize = 12
              ),
              widget.PulseVolume(
                  foreground = colors[2],
                  padding =3,
                  limit_max_volume = True
              ),
              widget.TextBox(
                       text = '|',
                       foreground = colors[7],
                       padding = 6,
                       font = "Source Code Pro",
                       fontsize = 12
                       ),
              widget.TextBox(
                       font = "mononoki Nerd Font",
                       text = " ",
                       foreground = colors[2],
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('gsimplecal')},
                       ),
              widget.Clock(
                       format = "  %a, %d %b  %H:%Mh ",
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('gsimplecal')},
                       ),
              widget.Image(
                       scale = True,
                       margin = 2,
                       filename = "~/.config/qtile/icons/power-button-off.png",
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(HOME+"/.config/rofi/powermenu/powermenu.sh"), 'Button3': lambda: qtile.cmd_spawn(HOME+"/.sources/xmenu/xmenu-power.sh")}
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       )
              ]
    return widgets_list

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    del widgets_screen1[20:27]          # Slicing removes unwanted widgets on monitor 2
    return widgets_screen1              # Monitor 1 will display the rest of widgets in widgets_list

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    del widgets_screen2[18]          # Slicing removes unwanted widgets on monitor 1
    del widgets_screen2[19:27]
    return widgets_screen2              # Monitor 2 will display the rest of widgets in widgets_list

def init_widgets_screen3():
    widgets_screen3 = init_widgets_list()
    del widgets_screen3[18:51]          # Slicing removes unwanted widgets on monitor 3
    return widgets_screen3              # Monitor 3 will display the rest of widgets in widgets_list

def init_screens():
     return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), background = "#1e1f2900", border_width = 0, border_color = "#1e1f2900", opacity=1.0, size=25, margin = [10,10,0,10])),
             Screen(top=bar.Bar(widgets=init_widgets_screen2(), background = "#1e1f2900", border_width = 0, border_color = "#1e1f2900", opacity=1.0, size=25, margin = [10,10,0,10])),
             Screen(top=bar.Bar(widgets=init_widgets_screen3(), background = "#1e1f2900", border_width = 0, border_color = "#1e1f2900", opacity=1.0, size=25, margin = [10,10,0,10]))]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()
    widgets_screen3 = init_widgets_screen3()

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

main = None
auto_fullscreen = True
focus_on_window_activation = "smart" # or "focus" or "never"
reconfigure_screens = True
dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = True
cursor_warp = False
main = None
# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = False

floating_layout = layout.Floating(**layout_theme, float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    # default_float_rules include: utility, notification, toolbar, splash, dialog,
    # file_progress, confirm, download and error.
    *layout.Floating.default_float_rules, # or:
        ### DEFAULT FLOATING RULES ###
        # Match(wm_type='utility'),
        # Match(wm_type='notification'),
        # Match(wm_type='toolbar'),
        # Match(wm_type='splash'),
        # Match(wm_type='dialog'),
        # Match(wm_class='file_progress'),
        # Match(wm_class='confirm'),
        # Match(wm_class='dialog'),
        # Match(wm_class='download'),
        # Match(wm_class='error'),
        # Match(wm_class='notification'),
        # Match(wm_class='splash'),
        # Match(wm_class='toolbar'),
        # Match(func=lambda c: c.has_fixed_size()),
        # Match(func=lambda c: c.has_fixed_ratio()),
    ### PERSONAL FLOATING RULES ###
    Match(title='Leafpad'),                     # leafpad text editor
    Match(title='Stacer'),                      # stacer
    Match(wm_class='Nm-connection-editor'),     # nvidia settings
    Match(wm_class='Yad'),                      # yad (keybidings help)
    Match(title='Confirmation'),                # tastyworks exit box
    Match(wm_class='pinentry-gtk-2')            # GPG key password entry
])

@hook.subscribe.client_new
def dialogs(window):
    if(window.window.get_wm_type() == 'dialog' or window.window.get_wm_transient_for()):
        window.floating = True

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

wmname = "LG3D"
